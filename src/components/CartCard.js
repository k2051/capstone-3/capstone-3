import {Button, Row, Col, Card} from 'react-bootstrap'

import {useState} from 'react'

import React from 'react'


export default function Item({itemProp}){
const{name,quantity,subtotal,productId}=itemProp
const [buttonQuantity,setButtonQuantity]=useState(quantity)

const [newSubtotal,setNewSubtotal]=useState(subtotal)
// const [totalAmount,setTotalAmount]=useState(0)

	
	fetch('https://safe-citadel-55721.herokuapp.com/users/getCart',{
			method:'GET',
			headers:{
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
	.then(res=>res.json())
	.then(data=>{
		if(data.products.length===0){
			localStorage.setItem('cartTotal',0)
		}else{
		localStorage.setItem('cartTotal',data.cartTotal)
		}
		})

const reduce=()=>{
	fetch('https://safe-citadel-55721.herokuapp.com/users/getCart/deleteProduct',{
			method:'DELETE',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId:productId
			})
		})
	.then(res=>res.json())
	.then(data=>{
		setButtonQuantity(data.quantity)
		setNewSubtotal(data.subtotal)
		// setTotalAmount(localStorage.setItem('totalAmount',data.total))
		localStorage.setItem('subtotal',data.subtotal)
		})
}
	
const add=()=>{
	fetch('https://safe-citadel-55721.herokuapp.com/users/addToCart',{
			method:'POST',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId:productId,
				quantity:1
			})
		})
	.then(res=>res.json())
	.then(data=>{
		setButtonQuantity(data.quantity)
		setNewSubtotal(data.subtotal)
		localStorage.setItem('subtotal',data.subtotal)
		// setTotalAmount(localStorage.setItem('totalAmount',data.total))
	
		})
}

const remove=()=>{
	fetch('https://safe-citadel-55721.herokuapp.com/users/getCart/removeProduct',{
			method:'DELETE',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId:productId
			})
		})
	.then(res=>res.json())
	.then(data=>{
		setButtonQuantity(data.quantity)
		setNewSubtotal(data.subtotal)
		// setTotalAmount(localStorage.setItem('totalAmount',data.total))
		localStorage.setItem('subtotal',data.subtotal)
		})
}

// console.log(buttonQuantity)
return (
		<Row>
			<Col>
				<Card className='p-3'>
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle>quantity:</Card.Subtitle>
				   	 <Card.Text>{buttonQuantity}</Card.Text>
				   	  <Button variant="primary" size='sm' onClick={reduce}>-</Button>
				   	  <Button variant="primary" size='sm' onClick={add}>+</Button>
				   	  <Button variant="danger" size='sm' onClick={remove}>Remove</Button>
				    <Card.Subtitle>Subtotal:</Card.Subtitle>
				     <Card.Text>PhP {newSubtotal}</Card.Text>

				     {/*<Card.Text>Enrollees: {count}</Card.Text>
				    <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
				    {/*<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>*/}
				  </Card.Body>
				</Card>

				
			</Col>
		</Row>
		)
}

