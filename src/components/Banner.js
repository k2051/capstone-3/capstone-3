// import Button from 'react-bootstrap/Button'
// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'
import { Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {useContext,Fragment} from 'react'
import UserContext from '../UserContext'


export default function Banner(props){
	const {user}=useContext(UserContext)

	// console.log(user)
	return(
			(props.value===true) ?
			
			(user.isAdmin) ?
			<Fragment>
			<Row>
				<Col className='p-5'>
					<h1>Welcome, Admin!</h1>
					<p>What do you want to do?</p>
					
				</Col>
			</Row>
			</Fragment>
			:
			<Fragment>
			<Row>
				<Col className='p-5'>
					<h1>Gadget Store</h1>
					<p>Gaming Peripherals for your Gaming needs</p>
					
				</Col>
			</Row>
			</Fragment>
			
			
			:
			<Fragment>
			<Row>
				<Col className='p-5'>
					<h1>Page Not Found</h1>
					<p>Go to <Link to='/'>homepage</Link></p>
				</Col>
			</Row>
			</Fragment>
		)
}

