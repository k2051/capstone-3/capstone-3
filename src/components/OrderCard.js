import {Row, Col, Card} from 'react-bootstrap'

import {useState,Fragment,useEffect} from 'react'

import React from 'react'


export default function Order({orderProp}){
const{purchasedOn,products}=orderProp

const [p,setP]=useState([])
// console.log(products)
	
	/*fetch('http://localhost:4000/users/myOrders',{
			method:'GET',
			headers:{
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
	.then(res=>res.json())
	.then(data=>{
		setTotalAmount(localStorage.setItem('totalAmount',data.totalAmount))
		})*/
useEffect(()=>{
	
	setP(products.map(p=>{

		return (
			<Row>
			<Col>
				<Card className='p-3'>
				  <Card.Body>
				    <Card.Title>{p.name}</Card.Title>
				  <Card.Subtitle>quantity:</Card.Subtitle>
				   	 <Card.Text>{p.quantity}</Card.Text>
				   
				    {/*<Card.Subtitle>Total:</Card.Subtitle>
				     <Card.Text>PhP {subtotal}</Card.Text>*/}
				     {/*<Card.Text>Enrollees: {count}</Card.Text>
				    <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
				    {/*<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>*/}
				  </Card.Body>
				</Card>

				
			</Col>
		</Row>
			)
	}))
	
},[products])


return (
		<Fragment>
		{p}
		


		<Row>
			<Col>
				<Card className='p-3'>
				  <Card.Body>
				  {/*  <Card.Title>{name}</Card.Title>*/}
				  
				    <Card.Subtitle>Purchased On:</Card.Subtitle>
				   	 <Card.Text>{purchasedOn}</Card.Text>
				   	  
				    {/*<Card.Subtitle>Total:</Card.Subtitle>
				     <Card.Text>PhP {subtotal}</Card.Text>*/}
				     {/*<Card.Text>Enrollees: {count}</Card.Text>
				    <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
				    {/*<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>*/}
				  </Card.Body>
				</Card>

				
			</Col>
		</Row>
		</Fragment>
		)
}

