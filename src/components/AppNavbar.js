import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {Link} from 'react-router-dom'
import {Fragment,useContext} from 'react'
import UserContext from '../UserContext'
import './AppNavbar.css'

export default function AppNavbar(){

  // const [user,setUser]=useState(localStorage.getItem('email'))
  // console.log(user)
  const {user}=useContext(UserContext)

  return(
    <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand className="mx-3" as={Link} to="/"><i className="fa-solid fa-cart-shopping px-2"></i>Shop</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto mx-3">
            <Nav.Link  as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Products</Nav.Link>
            {
              (user.id !== null) ?
              <Fragment>
              {(user.isAdmin) ? 
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
              <Fragment>
              <Nav.Link as={Link} to="/cart">Cart</Nav.Link>
              <Nav.Link as={Link} to="/orders">Orders</Nav.Link>
               <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              </Fragment>
              }
             
              
              </Fragment>
              :
              <Fragment>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
              </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
    </Navbar>
    )
}