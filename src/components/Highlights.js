import {Row,Col,Card} from 'react-bootstrap'
import {useContext,Fragment} from 'react'
import UserContext from '../UserContext'
import {Link} from 'react-router-dom'


export default function Highlights(){
const {user}=useContext(UserContext)

	return(
		(user.isAdmin)?
		<Fragment>
		<Row className='my-3'>
			<Col xs={12} md={4}>
				<Card className='cardHighlight p-3'>
				  <Card.Body>
				    <Card.Title>Update Product</Card.Title>
				    <Card.Text>Update a product from <Link to='/products/'>Product List</Link></Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			{/*<Col xs={12} md={3}>
				<Card className='cardHighlight p-3'>
				  <Card.Body>
				    <Card.Title>Get all orders</Card.Title>
				    <Card.Text>
				     get all order
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>*/}
			<Col xs={12} md={4}>
				<Card className='cardHighlight p-3'>
				  <Card.Body>
				    <Card.Title>Deactivate product</Card.Title>
				     <Card.Text>Deactivate a product from <Link to='/products/'>Product List</Link></Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className='cardHighlight p-3'>
				  <Card.Body>
				    <Card.Title>Create new product</Card.Title>
				    <Link to='/products/create'>Create new product</Link>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
		</Fragment>
		:
		<Fragment>
		<Row className='my-3'>
			<Col xs={12} md={6}>
				<Card className='cardHighlight p-3'>
				  <Card.Body>
				    <Card.Title>Headsets</Card.Title>
				    <Card.Text>
				     Various Headsets for gaming
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={6}>
				<Card className='cardHighlight p-3'>
				  <Card.Body>
				    <Card.Title>Gaming mice</Card.Title>
				    <Card.Text>
				     Various mice for gaming
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
		</Fragment>
		)
}