import {Button, Row, Col, Card} from 'react-bootstrap'
// import {useState, useEffect} from 'react'
import {Link,useParams} from 'react-router-dom'
import {useContext,Fragment,useState} from 'react'
import UserContext from '../UserContext'

export default function Product({productProp}){

	const{name,description,price,_id,isActive}=productProp
	const{user}=useContext(UserContext)
	const[status,setStatus]=useState(isActive)
	
	


	return (
		<Row>
			<Col>
				<Card className='productCard p-3'>
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle>Description:</Card.Subtitle>
				   	 <Card.Text>{description}</Card.Text>
				    <Card.Subtitle>Price:</Card.Subtitle>
				     <Card.Text>PhP {price}</Card.Text>
				     <Card.Subtitle>isActive:</Card.Subtitle>
				     <Card.Text>{status.toString()}</Card.Text>
{/*				     {
				     	(user.isAdmin)?
				     	<Fragment>
				     	<Link className="btn btn-primary" to={`/products/${_id}`}>Update</Link>
				     	<Button className="btn btn-primary" onClick={()=>archive(_id)}>Archive</Button>
				     	</Fragment>
				     	:
				     	<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
				     }*/}
				    <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
				  </Card.Body>
				</Card>
			</Col>
		</Row>


		)
}