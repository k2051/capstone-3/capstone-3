import {Form,Button,Row,Col,Container,Card} from 'react-bootstrap'
import {Navigate} from 'react-router-dom'
import {useState,useEffect,useContext} from 'react'
import Swal from 'sweetalert2'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import './Login.css'


export default function Login(){

	const {user,setUser}=useContext(UserContext)
	const [email,setEmail]=useState('')
	const [password,setPassword]=useState('')
	const [isActive,setIsActive]=useState(false)

	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	function loginUser(e){
		e.preventDefault()

		/*
			syntax:
				fetch('url',{options})
				.then(res=>res.json())
				.then(data=>{})
		*/
		fetch(`https://safe-citadel-55721.herokuapp.com/users/authenticate`,{
			method:'POST',
			headers:{
				'Content-type':'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			if (typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title:'Login Successful',
					icon:'success',
					text:'Welcome!'
				})
			} else {
				Swal.fire({
					title:'Authentication Failed',
					icon:'error',
					text:'Check your login details and try again'
				})
			}
		})

		// set the email from the authenticated user(logged in user) in the local storage
		/*
			syntax:
			localStorage.setItem('propertyName', value)
		*/
		// localStorage.setItem('email',email)

		// setUser({
		// 	email:localStorage.getItem('email')
		// })
		
		setEmail('')
		setPassword('')
		// alert('You are now logged in!')
	}

	const retrieveUserDetails=(token)=>{
		fetch('https://safe-citadel-55721.herokuapp.com/users/details',{
			method:'GET',
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if (email!=='' && password!=='') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	return (
		(user.id!==null)? 
		(user.isAdmin)?
		<Navigate to='/' />
		:

			<Navigate to ="/products"/>

		:
		

		<Container className="my-5 vh-100">
				<Row>
					<Col>
						<Card className="p-5 loginCard">
							<h2 className="text-center">Login</h2>
								<Form onSubmit={e=>loginUser(e)} className="my-5 text-center">
										  <Form.Group className="mb-3" controlId="userEmail">
										    <Form.Label>Email address</Form.Label>
										    <Form.Control 
										    				type="email"
										    				className="text-center"
										    				placeholder="Enter email"
										    				value={email}
										    				onChange= {e=>setEmail(e.target.value)}
										    				required />
										    <Form.Text className="text-muted">
										    
										    </Form.Text>
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="password1">
										    <Form.Label>Password</Form.Label>
										    <Form.Control 
										    				type="password" 
										    				className="text-center"
										    				placeholder="Password"
										    				value={password}
										    				onChange= {e=>setPassword(e.target.value)}
										    				required />
										  </Form.Group>
										
										  {
										  	isActive ?
										  	<Button variant="success" type="submit" id="submitBtn">
										  	  Login
										  	</Button>
										  	:
										  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
										  	  Login
										  	</Button>
										  }

										
					  
					  				<p className="text-center mt-5 mb-1">Don't have an account? <Link to='/register'>Register here</Link></p>
								</Form>
						</Card>
					</Col>
				</Row>
		</Container>
	)
}