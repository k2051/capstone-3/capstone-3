import {Form,Button,Row,Col,Container,Card} from 'react-bootstrap'
import {Navigate} from 'react-router-dom'
import {useState,useEffect,useContext} from 'react'
import Swal from 'sweetalert2'

import UserContext from '../UserContext'



export default function CreateProduct(){

	const {user}=useContext(UserContext)
	const [newName,setNewName]=useState('')
	const [newDescription,setNewDescription]=useState('')
	const [newPrice,setNewPrice]=useState(0)
	const [isActive,setIsActive]=useState(false)

	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	function create(e){
		e.preventDefault()

		/*
			syntax:
				fetch('url',{options})
				.then(res=>res.json())
				.then(data=>{})
		*/
		fetch(`https://safe-citadel-55721.herokuapp.com/products`,{
			method:'POST',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				name:newName,
				description:newDescription,
				price:newPrice				
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if (typeof data !== "undefined"){
				Swal.fire({
					title:'Creation Successful',
					icon:'success'
				})
			} else {
				Swal.fire({
					title:'Authentication Failed',
					icon:'error',
					text:'Check your login details and try again'
				})
			}
		})

		// set the email from the authenticated user(logged in user) in the local storage
		/*
			syntax:
			localStorage.setItem('propertyName', value)
		*/
		// localStorage.setItem('email',email)

		// setUser({
		// 	email:localStorage.getItem('email')
		// })
		
		
		// alert('You are now logged in!')
	}

	

	useEffect(()=>{
		if (newName!=='' && newDescription!==''&&newPrice!=='') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[newName,newDescription,newPrice])

	return(
		(user.isAdmin!==true)?
		<Navigate to='/'/>
		:
		<Container className="my-5 vh-100">
				<Row>
					<Col>
						<Card className="p-5 loginCard">
							<h2 className="text-center">Create Product</h2>
								<Form onSubmit={e=>create(e)} className="my-5 text-center">
										  <Form.Group className="mb-3" controlId="prodName">
										    <Form.Label>Name</Form.Label>
										    <Form.Control 
										    				type="text"
										    				className="text-center"
										    				placeholder="name"
										    				value={newName}
										    				onChange= {e=>setNewName(e.target.value)}
										    				/>
										    <Form.Text className="text-muted">
										    
										    </Form.Text>
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="desc">
										    <Form.Label>Description</Form.Label>
										    <Form.Control 
										    				type="text" 
										    				className="text-center"
										    				placeholder="description"
										    				value={newDescription}
										    				onChange= {e=>setNewDescription(e.target.value)}
										    				 />
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="price">
										    <Form.Label>Price</Form.Label>
										    <Form.Control 
										    				type="text" 
										    				className="text-center"
										    				placeholder="price"
										    				value={newPrice}
										    				onChange= {e=>setNewPrice(e.target.value)}
										    				 />
										  </Form.Group>
										
											  {
										  	isActive ?
										  	<Button variant="success" type="submit" id="submitBtn">
										  	  Create
										  	</Button>
										  	:
										  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
										  	 Create
										  	</Button>
										  }
								</Form>
						</Card>
					</Col>
				</Row>
		</Container>
	)
}