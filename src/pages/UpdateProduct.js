import {Form,Button,Row,Col,Container,Card} from 'react-bootstrap'
// import {Navigate} from 'react-router-dom'
import {useState,useEffect} from 'react'
import Swal from 'sweetalert2'
import {useParams,useNavigate} from 'react-router-dom'
// import UserContext from '../UserContext'

export default function UpdateProducts(productProp){
	// const{name,description,price,_id,isActive}=productProp
// const {user,setUser}=useContext(UserContext)

const {productId}=useParams()

const [name,setName]=useState('')
const [description,setDescription]=useState('')
const [status,setStatus]=useState(false)
const [price,setPrice]=useState(0)

const [newName,setNewName]=useState(name)
const [newDescription,setNewDescription]=useState(description)
const [newStatus,setNewStatus]=useState(status)
const [newPrice,setNewPrice]=useState(price)
	
	async function fetchProd(){
		const response=await fetch(`https://safe-citadel-55721.herokuapp.com/products/${productId}`)
		const prod=await response.json()
		return prod
	}
	
	fetchProd().then(prod=>{
		setName(prod.name)
		setDescription(prod.description)
		setPrice(prod.price)
		setStatus(prod.isActive)
	})
	

	
	const [isActive,setIsActive]=useState(false)

	// State that will be used to store the courses retrieved from the database

	// const [products,setProducts]=useState([])
	const navigate = useNavigate()
	

	
	function update(e){
		e.preventDefault()

		fetch(`https://safe-citadel-55721.herokuapp.com/products/${productId}`,{
			method:'PUT',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				name:newName,
				description:newDescription,
				price:newPrice,
				isActive:newStatus
			})
		})
		.then(res=>res.json())
		.then(data=>{
			 if(data){
			 	Swal.fire({
					title:'Update Successful',
					icon:'success'
				})
				navigate('/products')
			 } else {
			 	Swal.fire({
					title:'Update Failed',
					icon:'error',
					text:'Please try again'
				})

			 }
			
		})
	}

	// const update=(productId)=>{
	// 	fetch(`http://localhost:4000/products/${productId}`,{
	// 		method:'PUT',
	// 		headers:{
	// 			'Content-type':'application/json',
	// 			Authorization:`Bearer ${localStorage.getItem('token')}`
	// 		},
	// 		body:JSON.stringify({
	// 			name:newName,
	// 			description:newDescription,
	// 			price:newPrice,
	// 			isActive:status
	// 		})
	// 	})
	// 	.then(res=>res.json())
	// 	.then(data=>{
	// 		 if(data){
			 	
	// 			navigate('/products')
	// 		 } else {
	// 		 	Swal.fire({
	// 				title:'Update Failed',
	// 				icon:'error',
	// 				text:'Please try again'
	// 			})

	// 		 }
			
	// 	})
	// }

	useEffect(()=>{
		if (newName!=='' && (newDescription!==''||newPrice!==''||status!=='')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[newName,newDescription,newPrice,status])

	return(
		<Container className="my-5 vh-100">
				<Row>
					<Col>
						<Card className="p-5 loginCard">
							<h2 className="text-center">Update Product</h2>
								<Form onSubmit={e=>update(e)} className="my-5 text-center">
										  <Form.Group className="mb-3" controlId="prodName">
										    <Form.Label>Name</Form.Label>
										    <Form.Control 
										    				type="text"
										    				className="text-center"
										    				placeholder={name}
										    				value={newName}
										    				onChange= {e=>setNewName(e.target.value)}
										    				/>
										    <Form.Text className="text-muted">
										    
										    </Form.Text>
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="desc">
										    <Form.Label>Description</Form.Label>
										    <Form.Control 
										    				type="text" 
										    				className="text-center"
										    				placeholder={description}
										    				value={newDescription}
										    				onChange= {e=>setNewDescription(e.target.value)}
										    				 />
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="price">
										    <Form.Label>Price</Form.Label>
										    <Form.Control 
										    				type="text" 
										    				className="text-center"
										    				placeholder={price}
										    				value={newPrice}
										    				onChange= {e=>setNewPrice(e.target.value)}
										    				 />
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="stat">
										    <Form.Label>Status</Form.Label>
										    <Form.Control 
										    				type="text" 
										    				className="text-center"
										    				placeholder="status"
										    				value={newStatus}
										    				onChange= {e=>setNewStatus(e.target.value)}
										    				 />
										  </Form.Group>
										
											  {
										  	isActive ?
										  	<Button variant="success" type="submit" id="submitBtn">
										  	  Update
										  	</Button>
										  	:
										  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
										  	  Update
										  	</Button>
										  }
								</Form>
						</Card>
					</Col>
				</Row>
		</Container>
	)
		
	
}

