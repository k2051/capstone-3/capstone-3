import {Fragment,useEffect,useState} from 'react'
import {Button, Row, Col, Card} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
// import PropTypes from 'prop-types'
import CartCard from '../components/CartCard'
// import TotalAmountCard from '../components/TotalAmountCard'
// import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Cart(){
	const [items,setItems]=useState([])
	const [totalAmount,setTotalAmount]=useState(0)
	const navigate=useNavigate()
	
	const checkout=()=>{
		fetch('https://safe-citadel-55721.herokuapp.com/users/checkout',{
			method:'POST',
			headers:{
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			if((data!==undefined || data!==' ')&& data.products.length>0){
				Swal.fire({
					title:'Successfully ordered',
					icon:'success'
				})
				navigate('/orders')
			} else if ((data===undefined || data===' ')&& data.products.length<0){
				Swal.fire({
					title:'Something went wrong',
					icon:'error',
					text:'Please try again'
				})
			}
		})
	}

	useEffect(()=>{
		fetch('https://safe-citadel-55721.herokuapp.com/users/getCart',{
			method:'GET',
			headers:{
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			setTotalAmount(localStorage.getItem('cartTotal'))
			setItems(data.products.map(item=>{
				return (
					
					<CartCard key={item.productId} itemProp={item}/>
					
					
					)
			}))
		})

	},[items])



	return(

		<Fragment>
			{items}
		<Row>
			<Col>
				<Card>
				  <Card.Body>
				  	<Card.Subtitle>Total Amount:</Card.Subtitle>
				   	 <Card.Text>{totalAmount}</Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
		<Row>
			<Col className="text-center justify-content-center">

			 <Button variant="primary" size='sm' onClick={checkout}>Checkout</Button>
			 </Col>
		</Row>
		</Fragment>
		
		)

/*CartCard.propTypes={
	itemProp:PropTypes.shape({
		productId:PropTypes.string.isRequired,
		name:PropTypes.string.isRequired,
		quantity:PropTypes.number.isRequired,
		subtotal:PropTypes.number.isRequired
	})
}*/
}