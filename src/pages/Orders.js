import {Fragment,useEffect,useState} from 'react'



import OrderCard from '../components/OrderCard'

export default function Orders(){
	const [items,setItems]=useState([])



	useEffect(()=>{
		fetch('https://safe-citadel-55721.herokuapp.com/users/myOrders',{
			method:'GET',
			headers:{
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			// setTotalAmount(data.totalAmount)
			// console.log(d.concat())
			// console.log(d)
		
			// setItems(d.map(item=>{
			// 	return (
					
			// 		<OrderCard key={item.productId} itemProp={item}/>
					
					
			// 		)
			
			// }))
		
			setItems(data.map(item=>{
				return (
					
					<OrderCard key={item._id} orderProp={item}/>
					
					
					)
			
			}))
		
		})

	},[])



	return(

		<Fragment>
			{items}
		{/*<Row>
			<Col>
				<Card>
				  <Card.Body>
				  	<Card.Subtitle>Total Amount:</Card.Subtitle>
				   	 <Card.Text>{totalAmount}</Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>*/}
		</Fragment>
		
		)

// OrderCard.propTypes={
// 	orderProp:PropTypes.shape({
// 		_id:PropTypes.string.isRequired,
// 		products:PropTypes.array,
// 		purchasedOn:PropTypes.instanceOf(Date)
// 	})
// }
}