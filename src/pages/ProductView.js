import {useState,useEffect,useContext,Fragment} from 'react'
import {Container,Card,Row,Col,Button} from 'react-bootstrap'
import {useParams,useNavigate,Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ProductView(){

	const [name,setName]=useState('')
	const [description,setDescription]=useState('')
	const [price,setPrice]=useState(0)
	const quantity=1
	// the 'useParams' allows us to retrieve the courseId passed via the URL
	const {productId}=useParams()

	const {user}=useContext(UserContext)

	const navigate = useNavigate()

	const addToCart=productId=>{
		fetch('https://safe-citadel-55721.herokuapp.com/users/addToCart',{
			method:'POST',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId:productId,
				quantity:quantity
			})
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data.products.map(d=>d.total))
			
			if(data!==undefined){
				Swal.fire({
					title:'Successfully added to cart',
					icon:'success'
				})
				navigate('/cart')
			} else {
				Swal.fire({
					title:'Something went wrong',
					icon:'error',
					text:'Please try again'
				})
			}
		})
	}

	const archive=(productId)=>{
		// console.log(_id)
		fetch(`https://safe-citadel-55721.herokuapp.com/products/${productId}/archive`,{
			method:'PUT',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
	.then(res=>res.json())
	.then(data=>{
		navigate('/products')
	})
}


	useEffect(()=>{
		// console.log(productId)
		fetch(`https://safe-citadel-55721.herokuapp.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	},[productId])

	return(
		<Container className='mt-5'>
			<Row>
				<Col lg={{span:6,offset:3}}>
					<Card>
						<Card.Body className='text-center'>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							{/*<Card.Subtitle>Class Schedule:</Card.Subtitle>
							<Card.Text>8 AM - 5 PM</Card.Text>*/}
							{
								(user.id!==null) ?
								(user.isAdmin)?
								<Fragment>
								<Link className="btn btn-primary" to={`/products/${productId}/update`}>Update</Link>
				     			<Button className="btn btn-primary" onClick={()=>archive(productId)}>Archive</Button>
				     			</Fragment>
				     			:
								<Button variant="primary" onClick={()=>addToCart(productId)}>Add to Cart</Button>
								:
								<Link className="btn btn-danger" to="/login">Login to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)

}