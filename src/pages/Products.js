// import coursesData from '../data/coursesData'
import {Fragment,useEffect,useState,useContext} from 'react'
import ProductCard from '../components/ProductCard'
import UpdateProduct from '../pages/UpdateProduct'
import PropTypes from 'prop-types'
import UserContext from '../UserContext'

export default function Products(){
	// console.log(coursesData)
/*	const courses=coursesData.map(course=>{
		return(
		<CourseCard key={course.id} courseProp={course}/>
		)
	})*/

	// State that will be used to store the courses retrieved from the database
	const [products,setProducts]=useState([])


	const {user}=useContext(UserContext)
	useEffect(()=>{
		if(user.isAdmin){
			fetch('https://safe-citadel-55721.herokuapp.com/products/')
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			setProducts(data.map(product=>{
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
		}
		else{
		fetch('https://safe-citadel-55721.herokuapp.com/products/active')
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			setProducts(data.map(product=>{
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})

		}
	})

	return(

		<Fragment>
			{products}
		</Fragment>
		
		)

}

ProductCard.propTypes={
	productProp:PropTypes.shape({
		name:PropTypes.string.isRequired,
		description:PropTypes.string.isRequired,
		isActive:PropTypes.bool.isRequired,
		price:PropTypes.number.isRequired

	})
}

UpdateProduct.propTypes={
	productProp:PropTypes.shape({
		name:PropTypes.string.isRequired,
		description:PropTypes.string.isRequired,
		isActive:PropTypes.bool.isRequired,
		price:PropTypes.number.isRequired

	})
}