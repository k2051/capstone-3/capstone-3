import {useState,useEffect} from 'react'
import {UserProvider} from './UserContext'
// import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes,Route} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import ProductView from './pages/ProductView'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import UpdateProduct from './pages/UpdateProduct'
import CreateProduct from './pages/CreateProduct'
import './App.css'

function App() {

  const [user,setUser]=useState({
    // email:localStorage.getItem('email')
    id:null,
    isAdmin:null
  })

  // function to clear localstorage for logout
  const unsetUser=()=>{
    localStorage.clear()
  }

  

  useEffect(()=>{
    // console.log(user)
    // console.log(localStorage)
   
    fetch(`https://safe-citadel-55721.herokuapp.com/users/details`,{
      method:'GET',
      headers:{
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      if(typeof data._id!=='undefined'){
        // console.log(data._id)
        setUser({
          id:data._id,
          isAdmin:data.isAdmin
        })
      } else {
        setUser({
          id:null,
          isAdmin:null
        })
      }
    })
  },[])

  return (
  <UserProvider value={{user,setUser,unsetUser}}>  
    <Router>
      <AppNavbar />
      <Container>
      <Routes>
        <Route exact path="/" element={<Home/>} />
        <Route exact path="/products" element={<Products/>} />
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/logout" element={<Logout/>} />
        <Route exact path="/register" element={<Register/>} />
        <Route exact path="*" element={<Error />} />
        <Route exact path="/cart" element={<Cart/>} />
        <Route exact path='/products/:productId' element={<ProductView/>} />
        <Route exact path="/orders" element={<Orders/>} />
        <Route exact path='/products/:productId/update' element={<UpdateProduct/>} /> 
        <Route exact path='/products/create' element={<CreateProduct/>} />  

      </Routes>
      </Container>
    </Router>
  </UserProvider>    
  );
}

export default App;
